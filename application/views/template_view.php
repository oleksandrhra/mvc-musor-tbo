<?php
echo $_SESSION ['lang'];
?>
<!DOCTYPE HTML PUBLIC  "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<head>
    <meta charset="utf-8">
    <title>ТОВ "Комун-Трейд"</title>

    <!-- add styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="/css/styles.css" />
    <!-- add scripts -->
    <script src="/js/jquery-3.2.1.js" type="text/javascript"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/lang.js"></script>
</head>
<body >
<div class="main"><!-- start main -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="container">
                <div class="features"><!-- start feature -->
                    <div class="new_text">
                        <div class="col-lg-10 ">
                            <ul class="nav navbar-nav">
                                <ul class="nav navbar-nav header">
                                    <li>(050)-444-14-43</li>
                                    <li>(067)-388-17-11</li>

                                </ul>
                        </div>
                        <div class="col-lg-2">
                            <ul class="nav navbar-nav header">
                            <li>musor-tbo@ukr.net</li>
                            </ul>
                        </div>
                </div>

                    <div class="new_text">
                        <div class="col-lg-2 ">
                            <div class="navbar-header manu manu-img">
                                <a class="navbar-brand" href="main"><img src="/images/logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-10 ">
                            <ul class="nav navbar-nav manu">
                                <li><a href="main">Главная</a></li>
                                <li><a href="about">О компании</a></li>
                                <li><a href="services">Услуги</a></li>
                                <li><a href="product">Товары</a></li>
                                <li><a href="contact">Контакты</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<?php include 'application/views/'.$content_view; ?>

<style>
    html {
        position: relative;
        min-height: 100%;
    }

    body {
        margin-bottom: 60px;// значение высоты #footer
    }
    #footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 60px;
        background-color: #F0F0E9;
    }
    p{text-align:  center;}
</style>
<footer id="footer">
<div class="footer-bottom" style="background: #333">
    <div class="container">
        <div class="row" style="color: #E0E0F3"><br>
            <div class="col-md-12" style="color: aliceblue">
                <p><span>© Мусора нет 2018:</span></p>
                <p>Разработано Олександром Храбаном</p>
            </div>
        </div>
    </div>
</div>
</footer><!--/Footer-->
</body>
</html>