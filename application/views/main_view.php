<div class="main"><!-- start main -->
    <div class="container" style="background: azure">
        <br>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="/images/slider_1.png" alt="Los Angeles">
                </div>

                <div class="item">
                    <img src="/images/slider_1.png" width="100%" alt="Chicago">
                </div>

                <div class="item">
                    <img src="/images/slider_1.png" alt="New York">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>


        <div class="features"><!-- start feature -->
            <div class="new_text">


                <div class="col-sm-12 text">
                    <h2>Cпеціалізоване підприємство пропонує послуги в сфері комплексного поводження з відходами:</h2>
                    <img class="img-right" src="/images/title-img1.png">

                    <p><b>&bull; Вивіз твердих побутових відходів </b></p><br>
                    <p><b>&bull; Вивіз негабаритних та будівельних відходів </b></p><br>
                    <p><b>&bull; Продаж сміттєвих контейнерів</b></p><br>
                    <p><b>&bull; Закупка окремих видів ресурсноцінних відходів, як вторинної сировини (макулатура,
                            використана полімерна, скляна, металева тара та упаковка, плівка та інше)</b></p><br>
                    <p><b>&bull; Утилізація зіпсованих, непридатних для використання та прострочених виробів (харчової
                            продукції, косметичної продукції, побутової хімії, засобів гігієни та ін.)</b></p><br>
<p><em>Для забезпечення своєї діяльності та надання якісних послуг наше підприємство має кваліфікований персонал з великим досвідом роботи, , надійну сучасну техніку європейського виробництва та всі необхідні дозвільні документи для здійснення діяльності в сфері поводження з відходами.
    </em></p>
                </div>
            </div>
        </div>
    </div>
</div>'
<br>