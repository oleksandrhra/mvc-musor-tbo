<div class="main"><!-- start main -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="container">
                <div class="features"><!-- start feature -->
                    <div class="new_text">
                        <div  class="col-lg-11 ">
                            <div class="header">
                                <a class="navbar-brand" href="main">Приложение-задачник:</a>
                            </div>
                            <ul class="nav navbar-nav">
                                <li><a href="main">Главная</a></li>
                                <li class="active"><a href="new_task">Добавить новую задачу</a></li>
                            </ul>
                        </div>
                        <div  class="col-lg-1 ">
                            <ul class="nav navbar-nav">
                                <li><a href="admin.php">admin</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<style>
    #user {
        width: 800px; /* Ширина поля в пикселах */
    }
</style>

<div class="main"><!-- start main -->
    <div class="container" style="background: azure">
        <div class="features"><!-- start feature -->
            <div class="new_text">
                    <div class="col-sm-12">
                    <div class="table-responsive">

                        <h1>Добавь задачу прямо сейчас</h1>
                        <label>Загрузить фотографию:</label>
                        <form action="new_task/download" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                            <input id ="user"  type="file" name="picture">
                            <br>
                            <label>Тип загрузки</label>
                            <br>
                            <select name="file_type">
                                <option value="1">Эскиз</option>
                                <option value="2">Большое изображение</option>
                            </select>
                            <br>
                                <label>Имя:</label>
                                <input id ="user" name="name" type="text" class="form-control" placeholder="Имя"/>
                                <p class="help-block">Введите имя на русском языке</p>
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input id ="user" name="email" type="email" class="form-control" placeholder="E-mail"/>
                            </div>

                            <br>
                            <br>
                            <label>Текста задачи:</label>
                            <div class="form-group">
                                <textarea id ="user" name="comment" class="form-control"  cols="40" rows="3" placeholder="Сообщение"></textarea>
                            </div>
                        <br>
                        <div class="form-group" >
                            <input  type="submit" class="btn btn-info" value="Отправить">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>