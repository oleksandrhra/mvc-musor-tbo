<?php
/**
 * Created by PhpStorm.
 * User: Oleksandr
 * Date: 01.05.2018
 * Time: 15:51
 */
include ('connection.php');
class model_admin extends Model
{

    function get_autification()
    {
        $errors = array();
        $mysql = connectPDO();
        $query = $mysql->prepare('SELECT * FROM user WHERE login = :login');
        $_SESSION['login'] = $query['login'];
        $_SESSION['password'] = $query[0]['password'];
        $_SESSION['law'] = $query[0]['law'];
        $query->execute([':login' => $_SESSION['login']]);
        $resultQuery = $query->fetchAll();
        if ($_SESSION['login'] != $resultQuery[0]['login']) {
            $errors[] = "Користувача не знайдено";
        }
        if ($_SESSION['password'] != $resultQuery[0]['password']) {
            $errors[] = "Пароль не співпадає";
        }
        return $errors;
    }
}

